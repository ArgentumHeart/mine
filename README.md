# group 355, sem 4, 2015
Необходимо определится с темой вашей курсовой работы до 5 марта. Можно писать какую нибудь игру, физическую модель, ограничены только своей фантазией.

Дз: проверка скобочных выражений({[][]}{}) на корректность используя stack

Посещаемость:
http://goo.gl/8Z2QRg

Важные ссылки:
- Туториал по C++
http://www.cplusplus.com/doc/tutorial/
- Альтернативный туториал по C++
http://www.tutorialspoint.com/cplusplus/index.htm
- Слегка тормозной онлайн туториал по C++(нужно зарегистрироваться)
http://www.programmr.com/zone/cpp


```bash
# install
yum install -y gcc-c++
# clone repository
git clone https://bitbucket.org/snorch/spring-miptcs.git
# show log
git log
# show commit by commit ID
git show 13c541ed7f3
# compile
g++ -o test test.c
```
