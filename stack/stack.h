const int stack_granulariry = 10;

class Stack {
private:
	int *bp;
	int sp;
	int size;

	void stack_realloc(int size);
public:
	/*
	 * Standard constructor(default size = 10)
	 */
	Stack(int size = stack_granulariry);
	/*
	 * Copy constructor
	 */
	Stack(const Stack& s);

	bool is_full() { return sp >= size; }
	bool is_empty() { return sp <= 0; }

	/*
	 * Insert number to stack
	 */
	void push(int number);
	int compare(int number);
	/*
	 * Get number from stack
	 */
	int pop();
};

